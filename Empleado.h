#include <iostream>
#include "Fecha.h"
#include <string>
using namespace std;

class Empleado
{
private:
	string nombreEmp;
	float sueldoEmp;
	int categoriaEmp;
	Fecha cumpleAniosEmp;
	float porcentaje;
public:
	Empleado();
	//~Empleado();
	string getNombreEmp();
	float getSueldoEmp();
	int getCategoriaEmp();
	Fecha getFecha();
	void setNombreEmp(string);
	void setCategoriaEmp(int);
	void setSueldo(float);
	void setFecha(Fecha);
	void promoverEmpleado(int);
};

