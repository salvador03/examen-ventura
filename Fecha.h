#include <iostream>
#include <string>
#ifndef Fecha_HEADER
#define Fecha_HEADER
using namespace std;

class Fecha
{
private:
	int dia;
	int mes;
public:
	Fecha();
	int getDia();
	int getMes();
	void setDia(int);
	void setMes(int);
};

#endif