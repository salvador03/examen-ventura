#include <iostream>
#include "Empleado.h"
#include "Fecha.h"
#include <string>
using namespace std;

int main()
{
    Fecha cumplePepe;
    cumplePepe.setDia(19);
    cumplePepe.setMes(5);
    Fecha cumpleRocio;
    cumpleRocio.setDia(1);
    cumpleRocio.setMes(1);

    Empleado emp1;
    emp1.setNombreEmp("Pepe");
    emp1.setSueldo(10000);
    emp1.setCategoriaEmp(1);
    emp1.setFecha(cumplePepe);

    Empleado emp2;
    emp2.setNombreEmp("Rocio");
    emp2.setSueldo(12000);
    emp2.setCategoriaEmp(2);
    emp2.setFecha(cumpleRocio);
    
    cout << emp1.getNombreEmp() << ": " <<"Categoria: "<< emp1.getCategoriaEmp() << ", " <<"Sueldo: " << emp1.getSueldoEmp() << ", " << "Cumpleanos: " << emp1.getFecha().getDia() <<"/" <<"0" << emp1.getFecha().getMes() << endl;
    cout << emp2.getNombreEmp() << ": " <<"Categoria: "<<emp2.getCategoriaEmp() << ", " <<"Sueldo: " << emp2.getSueldoEmp() << ", " << "Cumpleanos: " <<"0" << emp2.getFecha().getDia() << "/" << "0" << emp2.getFecha().getMes() << endl;
    cout << endl;
    emp1.promoverEmpleado(4);
    cout << emp1.getNombreEmp() << ": " << "Categoria: " << emp1.getCategoriaEmp() << ", " << "Sueldo: " << emp1.getSueldoEmp() << endl;
    cout << endl;
    cout << "Cumpleanos de Rocio: " <<"0" << emp2.getFecha().getDia() << "/" << "0" << emp2.getFecha().getMes() << endl;
}

